# varlet-cli-app

### Reference

[@varlet/cli 中文文档](https://github.com/varletjs/varlet/blob/dev/packages/varlet-cli/README.md)
[@varlet/cli documentation](https://github.com/varletjs/varlet/blob/dev/packages/varlet-cli/README.en-US.md)

### Quickstart


#### 安装依赖 
```
pnpm install
```

#### 运行项目
```
pnpm dev
```

#### 构建文档站点
```
pnpm build
```
#### 预览文档站点
```
pnpm preview
```

#### 构建组件库代码
```
pnpm compile
```

#### 执行所有的单元测试
```
pnpm text
```

#### 以 watch 模式执行单元测试
```
pnpm test:watch 
or
pnpm test:watchAll 
```

#### 发布组件库
```
pnpm release
```

#### 检查代码
```
pnpm lint
```

#### 生成更新日志
```
pnpm changelog
```

## 文档结构配置
```js
module.exports = {
  pc: {
    redirect: '/home',
    title: {
      'zh-CN': 'kepler-ui',
    },
    header: {
      darkMode: null,
      i18n: null,
      github: 'https://github.com/varletjs/varlet',
    },
    menu: [
      {
        text: {
          'zh-CN': '开发指南',
        },
        // 侧边栏菜单目录
        type: 1,
      },
      {
        text: {
          'zh-CN': '基本介绍',
        },
        doc: 'home',
        // 索引项目根目录下的md文档
        type: 3,
      },
      {
        text: {
          'zh-CN': '基础组件',
        },
        type: 1,
      },
      {
        text: {
          'zh-CN': 'Button 按钮',
        },
        doc: 'button',
        // 索引组件根目录下的md文档
        type: 2,
      },
    ],
  },
  mobile: {
    redirect: '/home',
    title: {
      'zh-CN': 'kepler-ui',
    },
    header: {
      darkMode: null,
      i18n: null,
      playground: null,
      github: 'https://github.com/varletjs/varlet',
    },
  },
}
```

