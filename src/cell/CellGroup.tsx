import { defineComponent } from 'vue'
import './cell.less'

export default defineComponent({
  name: 'CellGroup',
  props: {
    inset: {
      type: Boolean,
    },
    title: {
      type: String,
    },
  },
  setup(props, { emit, slots }) {
    // 动态修改样式
    return () => {
      const { inset, title } = props
      const classType = (inset) => {
        let name = 'cell-group'
        if (inset) {
          name = 'cell-group cell-group__inset'
        }
        return name
      }

      return (
        <>
          <div>{title ? <div class="cell-group-title">{title}</div> : null}</div>
          <div class={classType(inset)}>{slots.default ? slots.default() : null}</div>
        </>
      )
    }
  },
})
