import { defineComponent } from 'vue'
import './cell.less'
import '../icon/icon.less'

export default defineComponent({
  name: 'Cell',
  props: {
    title: {
      type: String,
    },
    value: {
      type: String || Number,
    },
    label: {
      type: String,
    },
    border: {
      type: Boolean,
    },
    leftIcon: {
      type: String,
    },
    rightIcon: {
      type: String,
    },
  },
  setup(props, { emit, slots }) {
    // 动态修改样式
    return () => {
      const { title, value, label, leftIcon, rightIcon } = props
      return (
        <>
          <div class="cell">
            <div class="cell-container">
              <div class="cell-container__view">
                <div class="cell-container__view-left">
                  {leftIcon ? <i class={['icon', 'kp-' + leftIcon]}></i> : null}
                  {slots.title ? (
                    slots.title()
                  ) : title ? (
                    <div class="cell-container__view-right-title">{title}</div>
                  ) : null}
                </div>
                <div class="cell-container__view-right">
                  {rightIcon ? <i class={['icon', 'kp-' + rightIcon]}></i> : null}
                  {slots.value ? (
                    slots.value()
                  ) : value ? (
                    <div class="cell-container__view-right-value">{value}</div>
                  ) : null}
                </div>
              </div>
              {slots.label ? slots.label() : label ? <div class="cell-container__view-label">{label}</div> : null}
            </div>
          </div>
        </>
      )
    }
  },
})
