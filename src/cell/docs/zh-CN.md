# 按钮

### 引入

```js
import { createApp } from 'vue'
import { Cell,CellGrounp } from 'basic-ui'

createApp().use(Cell).use(CellGrounp)
```

### 基本使用
```html
  <CellGroup>
    <cell title="管理员" value="张三"></cell>
    <cell title="管理员" value="张三"></cell>
  </CellGroup>
```

### 圆角模式
```html
 <CellGroup inset>
    <cell title="管理员" value="张三"></cell>
    <cell title="管理员" value="张三"></cell>
  </CellGroup>
```

### 展示图标
```html
  <CellGroup>
    <cell title="管理员" value="张三" left-icon="dingwei"></cell>
  </CellGroup>
```

### 使用插槽
```html
 <CellGroup>
    <cell value="张三" label="手机号">
      <template #title>
        <div>
          用户名
        </div>
      </template>
    </cell>
  </CellGroup>
```
### 分组
```html
 <CellGroup title="分组一">
    <cell title="管理员" value="张三"></cell>
    <cell title="管理员" value="张三"></cell>
  </CellGroup>
  <CellGroup title="分组二">
    <cell title="管理员" value="张三"></cell>
    <cell title="管理员" value="张三"></cell>
  </CellGroup>
```


## API

### CellGroup

| 参数 | 说明 | 类型 | 默认值 | 
| --- | --- | --- | --- | 
| `title` | 分组名称 | _string_ | `default` |

###  Cell

| 参数 | 说明 | 类型 | 默认值 | 
| --- | --- | --- | --- | 
| `title` | 左侧标题 | _string_ | `default` |
| `value` | 右侧内容 | _boolean_ | `false` |
| `left-icon` | 左侧图标 | _boolean_ | `false` |
| `label` | 标题下方的描述信息 | _boolean_ | `default` |

### 插槽

| 插槽名 | 说明 | 参数 |
| --- | --- | --- |
| `title` | 自定义左侧标题 | `-` |
| `value` | 自定义右侧内容 | `-` |
| `label` | 自定义标题下方的描述信息 | `-` |
