import { defineComponent, ref } from 'vue'
import './Input.less'
import '../icon/Icon.less'

function typeValidator(type: string): boolean {
  return ['text', 'password', 'number', 'digit', 'text', 'tel', 'range', 'email'].includes(type)
}

export default defineComponent({
  name: 'BInput',
  props: {
    type: {
      type: String,
      default: 'text',
      validator: typeValidator,
    },
    label: {
      type: String,
    },
    modelValue: {
      type: String || Number,
    },
    width: {
      type: String || Number,
    },
    placeholder: {
      type: String,
    },
    clearable: {
      type: Boolean,
    },
  },
  setup(props, { emit }) {
    const clear = ref(props.clearable)
    clear.value = false
    // 双向绑定
    const updateModelValue = ($event) => {
      emit('update:modelValue', $event.target.value)
    }
    const updateFoucs = ($event) => {
      console.log(clear.value, props.clearable)
      if (props.clearable) {
        clear.value = true
      }
      emit('foucs', $event.target.value)
    }
    const updateBlur = ($event) => {
      clear.value = false
      console.log(clear.value, '===11===')
      emit('blur', $event.target.value)
    }
    const classTypeReturn = (props) => {
      console.log(props)
      let className = 'b-input-view__control'
      return className
    }
    // 动态修改样式
    return () => {
      const { type, modelValue, label, width, placeholder } = props
      console.log(clear.value, '==clear==')
      return (
        <>
          <div class="b-input">
            <div class="b-input-label" style={{ display: !label ? 'none' : 'visible' }}>
              <label class="label">{label}</label>
            </div>
            <div class="b-input-view" style={{ width }}>
              <input
                placeholder={placeholder}
                class={[classTypeReturn(props)]}
                type={type}
                value={modelValue}
                onBlur={(e) => updateBlur(e)}
                onFocus={(e) => updateFoucs(e)}
                onInput={(e: Event) => updateModelValue(e)}
              />
              {clear.value ? <i class={['icon', 'clearable']}></i> : ''}
            </div>
          </div>
        </>
      )
    }
  },
})
