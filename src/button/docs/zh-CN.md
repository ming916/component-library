# 按钮

### 引入

```js
import { createApp } from 'vue'
import { Button } from 'basic-ui'

createApp().use(Button)
```

### 基本使用
```html
<b-button>基本使用</b-button>
```

### 主题色按钮
##### 按钮颜色支持渐变色
```html
<b-button color="rgba(115, 145, 197, 1)">主题色按钮</b-button>
<b-button color="rgba(199, 177, 189, 1)"  plain >单色按钮</b-button>
```

### 按钮样式
```html
<b-button circular>圆形按钮</b-button>
<b-button square>方形按钮</b-button>
```

### 页面导航
##### 两种导航方式，一种是url一种是路由
```html
<b-button url="https://www.baidu.com/">url转跳</b-button>
<b-button to="icon">路由转跳</b-button>
```

### 图标按钮
##### 通过 icon 属性设置按钮图标，支持 Icon 组件里的所有图标，也可以传入图标 URL。
```html
 <b-button icon="xinjian" icon-size="20" icon-color="rgba(115, 145, 197, 1)" color="#03A9F4">
   按钮</b-button>
 <b-button icon-size="20" icon-color="rgba(199, 177, 189, 1)" icon="xinjian" >
   按钮</b-button>
```

### 禁用状态
```html
<b-button color="rgba(115, 145, 197, 1)" circular disabled>禁用状态</b-button>
<b-button square disabled>禁用状态</b-button>
```

### 按钮大小
```html
<b-button size="large" color="rgba(115, 145, 197, 1)">大号按钮</b-button>
<b-button size="normal" color="rgba(115, 145, 197, 1)" >普通按钮</b-button>
<b-button size="small" color="rgba(115, 145, 197, 1)" >小型按钮</b-button>
<b-button size="mini" color="rgba(115, 145, 197, 1)" >迷你按钮</b-button>
```


## API

### 属性

| 参数 | 说明 | 类型 | 默认值 | 
| --- | --- | --- | --- | 
| `color` | 按钮颜色 | _string_ | `default` |
| `circular` | 是否为圆形按钮 | _boolean_ | `false` |
| `square` | 是否为方形按钮 | _boolean_ | `false` |
| `url` | url跳转的路径 | _boolean_ | `default` |
| `to` | 路由跳转的地址 | _boolean_ | `default` |
| `icon` | 图标 | _string_ | `default` |
| `icon-size` | 图标大小 | _boolean_ | `30` |
| `icon-color` | 图标颜色 | _boolean_ | `#000` |
| `disabled` | 是禁用按钮 | _boolean_ | `false` |
| `size` | 按钮大小 | _string_ | `normal` |


### 事件

| 事件名 | 说明 | 参数 |
| --- | --- | --- |
| `click` | 点击按钮时触发 | `event: Event` |

### 插槽

| 插槽名 | 说明 | 参数 |
| --- | --- | --- |
| `default` | 按钮内容 | `-` |