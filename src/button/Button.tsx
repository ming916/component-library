import { defineComponent, PropType } from 'vue'
import './button.less'
import '../icon/icon.less'
import { useRouter } from 'vue-router'

function typeValidator(type: string): boolean {
  return ['default', 'primary', 'info', 'success', 'warning', 'danger'].includes(type)
}
function sizeValidator(size: string): boolean {
  return ['normal', 'mini', 'small', 'large'].includes(size)
}
export default defineComponent({
  name: 'BButton',
  props: {
    color: {
      type: String,
    },
    circular: {
      type: Boolean,
    },
    square: {
      type: Boolean,
    },
    disabled: {
      type: Boolean,
    },
    plain: {
      type: Boolean,
    },
    type: {
      type: String as PropType<'default' | 'primary' | 'info' | 'success' | 'warning' | 'danger'>,
      default: 'default',
      validator: typeValidator,
    },
    size: {
      type: String as PropType<'normal' | 'mini' | 'small' | 'large'>,
      default: 'normal',
      validator: sizeValidator,
    },
    icon: {
      type: String,
    },
    iconSize: {
      type: String,
    },
    iconColor: {
      type: String,
    },
    url: {
      type: String,
    },
    to: {
      type: String,
    },
    onClick: {
      type: Function as PropType<(e: Event) => void>,
    },
  },
  setup(props, { slots }) {
    const router = useRouter()
    const handleClick = (e: Event) => {
      if (props.url) {
        window.location.href = props.url
      }
      if (props.to) {
        router.push({
          path: props.to,
        })
      }
      props.onClick?.(e)
    }
    // 动态修改样式
    const handType = (item) => {
      let className = 'basic-button'
      // 圆形
      if (item.circular) {
        className += ' basic-button-circular'
      }
      // 方形
      if (item.square) {
        className += ' basic-button-square'
      }
      // 禁止点击
      if (item.disabled) {
        className += ' basic-button-disabled'
      }
      // 单色按钮
      if (item.plain) {
        className += ' basic-button-plan'
      }
      // 按钮大小
      if (item.size) {
        className += ` basic-button--${item.size}`
      }
      if (item.iconSize || item.iconColor || item.icon) {
        className += ` basic-button--icon`
      }
      return className
    }
    return () => {
      const { color, plain, icon, iconSize, iconColor } = props

      return (
        <>
          <button
            class={[handType(props)]}
            style={{
              background: plain ? '' : color,
              color: plain ? color : '',
              borderColor: plain ? color : '',
            }}
            onClick={handleClick}
          >
            <i
              style={{ display: icon ? 'block' : 'flex', fontSize: iconSize + 'px', color: iconColor }}
              class={['icon', 'kp-' + icon]}
            ></i>
            {slots.default?.()}
          </button>
        </>
      )
    }
  },
})
