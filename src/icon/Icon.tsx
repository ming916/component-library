import { defineComponent, PropType } from 'vue'
import './icon.less'

export default defineComponent({
  name: 'BIcon',
  props: {
    name: {
      type: String,
    },
    size: {
      type: String,
    },
    color: {
      type: String,
    },
    onClick: {
      type: Function as PropType<(e: Event) => void>,
    },
  },
  setup(props, { slots }) {
    const handleClick = (e: Event) => props.onClick?.(e)
    return () => {
      const { name, size, color } = props
      return (
        <>
          <i onClick={handleClick} class={['icon', 'kp-' + name]} style={{ fontSize: size + 'px', color: color }}></i>
        </>
      )
    }
  },
})
