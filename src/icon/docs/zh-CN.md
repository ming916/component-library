# 按钮

### 引入

```js
import { createApp } from 'vue'
import { Icon } from 'basic-ui'

createApp().use(Icon)
```

### 基本使用
```html
<b-icon name="kp-huandengpian"></b-icon>
```

### 图标颜色
```html
<b-icon name="kp-huandengpian" color='rgba(199, 177, 189, 1)'></b-icon>
<b-icon name="kp-quanxian" color='rgba(233, 174, 51, 1)'></b-icon>
```

### 图标大小
```html
<b-icon name="kp-huandengpian" color='rgba(199, 177, 189, 1)' size='40'></b-icon>
<b-icon name="kp-quanxian" color='rgba(233, 174, 51, 1)' size='30'></b-icon>
```

### 禁用状态
```html
<b-button color="#03A9F4" circular disabled>禁用状态</b-button>
<b-button square disabled>禁用状态</b-button>
```


## API

 ### 属性

 | 参数 | 说明 | 类型 | 默认值 | 
 | --- | --- | --- | --- | 
 | `color` | 图标颜色 | _string_ | `default` |
 | `size` | 图标大小 | _string_ | `30` |


### 事件

| 事件名 | 说明 | 参数 |
| --- | --- | --- |
| `click` | 点击按钮时触发 | `event: Event` |
