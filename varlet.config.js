/**
 * @                       _oo0oo_
 * @                      o8888888o
 * @                      88" . "88
 * @                      (| -_- |)
 * @                      0\  =  /0
 * @                    ___/`---'\___
 * @                  .' \\|     |// '.
 * @                 / \\|||  :  |||// \
 * @                / _||||| -:- |||||- \
 * @               |   | \\\  - /// |   |
 * @               | \_|  ''\---/''  |_/ |
 * @               \  .-\__  '-'  ___/-. /
 * @             ___'. .'  /--.--\  `. .'___
 * @          ."" '<  `.___\_<|>_/___.' >' "".
 * @         | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 * @         \  \ `_.   \_ __\ /__ _/   .-` /  /
 * @     =====`-.____`.___ \_____/___.-`___.-'=====
 * @                       `=---='
 * @
 * @
 * @     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * @
 * @           佛祖保佑     永不宕机     永无BUG
 * @
 * @       佛曰:
 * @               写字楼里写字间，写字间里程序员；
 * @               程序人员写程序，又拿程序换酒钱。
 * @               酒醒只在网上坐，酒醉还来网下眠；
 * @               酒醉酒醒日复日，网上网下年复年。
 * @               但愿老死电脑间，不愿鞠躬老板前；
 * @               奔驰宝马贵者趣，公交自行程序员。
 * @               别人笑我忒疯癫，我笑自己命太贱；
 * @               不见满街漂亮妹，哪个归得程序员？
 * @
 * @Author: kepler_明
 * @Date: 2022-05-17 10:22:03
 * @LastEditors: kepler_明
 * @LastEditTime: 2022-06-23 09:57:02
 * @FilePath: varlet.config.js
 * @Copyright (c) 2022 by kepler_明, All Rights Reserved.
 */

module.exports = {
  name: 'Basic',
  namespace: 'b',
  title: 'Basic UI',
  logo: './logo.svg',
  useMobile: true,
  themes: {
    'color-body': '#fff',
    'color-bar': '#fff',
    'color-sub-bar': '#f5f5f5',
    'color-text': '#555',
    'color-sub-text': '#888',
    'color-border': 'rgba(0, 0, 0, 0.12)',
    'color-shadow': '#eee',
    'color-introduce-border': '#009688',
    'color-primary': '#009688',
    'color-link': '#009688',
    'color-type': '#009688',
    'color-progress': '#009688',
    'color-progress-track': '#fff',
    'color-side-bar': '#009688',
    'color-side-bar-active-background': '#00968821',
    'color-app-bar': '#009688',
    'color-nav-button-hover-background': 'rgba(0, 0, 0, 0.08)',
    'color-pc-language-active': '#009688',
    'color-pc-language-active-background': '#00968821',
    'color-mobile-language-active': '#009688',
    'color-mobile-language-active-background': '#00968821',
    'color-mobile-cell-hover': '#009688',
  },
  darkThemes: {
    'color-body': '#121212',
    'color-bar': '#1e1e1e',
    'color-sub-bar': '#272727',
    'color-text': '#fff',
    'color-sub-text': '#aaa',
    'color-border': '#333',
    'color-shadow': '#121212',
    'color-introduce-border': '#555',
    'color-primary': '#009688',
    'color-link': '#009688',
    'color-type': '#009688',
    'color-progress': '#009688',
    'color-progress-track': '#202020',
    'color-side-bar': '#009688',
    'color-side-bar-active-background': '#00968821',
    'color-app-bar': '#009688',
    'color-nav-button-hover-background': 'rgba(255, 255, 255, 0.08)',
    'color-pc-language-active': '#009688',
    'color-pc-language-active-background': '#00968821',
    'color-mobile-language-active': '#009688',
    'color-mobile-language-active-background': '#00968821',
    'color-mobile-cell-hover': '#009688',
  },
  highlight: {
    style: './highlight.css',
  },
  pc: {
    redirect: '/home',
    title: {
      'zh-CN': 'kepler-ui',
    },
    header: {
      i18n: null,
      playground: null,
      github: 'https://github.com/varletjs/varlet',
    },
    menu: [
      {
        text: {
          'zh-CN': '开发指南',
        },
        type: 1,
      },
      {
        text: {
          'zh-CN': '基本介绍',
        },
        doc: 'home',
        type: 3,
      },
      {
        text: {
          'zh-CN': '基础组件',
        },
        type: 1,
      },
      {
        text: {
          'zh-CN': 'Button 按钮',
        },
        doc: 'button',
        type: 2,
      },
      {
        text: {
          'zh-CN': 'Icon 图标',
        },
        doc: 'icon',
        type: 2,
      },
      {
        text: {
          'zh-CN': 'Cell 单元格',
        },
        doc: 'cell',
        type: 2,
      },
      {
        text: {
          'zh-CN': 'Input 输入框',
        },
        doc: 'input',
        type: 2,
      },
    ],
  },
  mobile: {
    redirect: '/home',
    title: {
      'zh-CN': 'kepler-ui',
    },
    header: {
      i18n: null,
      github: 'https://github.com/varletjs/varlet',
    },
    menu: [
      {
        text: {
          'zh-CN': '开发指南',
        },
        type: 1,
      },
      {
        text: {
          'zh-CN': '基本介绍',
        },
        doc: 'home',
        type: 3,
      },
      {
        text: {
          'zh-CN': '基础组件',
        },
        type: 1,
      },
      {
        text: {
          'zh-CN': 'Button 按钮',
        },
        doc: 'button',
        type: 2,
      },
      {
        text: {
          'zh-CN': 'Icon 图标',
        },
        doc: 'icon',
        type: 2,
      },
      {
        text: {
          'zh-CN': 'Cell 单元格',
        },
        doc: 'cell',
        type: 2,
      },
      {
        text: {
          'zh-CN': 'Input 输入框',
        },
        doc: 'input',
        type: 2,
      },
    ],
  },
}
